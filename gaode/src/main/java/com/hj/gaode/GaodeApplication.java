package com.hj.gaode;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient//开启Eureka客户端功能
@SpringBootApplication(scanBasePackages = "com.hj")
public class GaodeApplication {

	public static void main(String[] args) {
		SpringApplication.run(GaodeApplication.class, args);
		System.out.println("启动成功 !!!");
	}

}
