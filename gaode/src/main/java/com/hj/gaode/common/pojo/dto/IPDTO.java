package com.hj.gaode.common.pojo.dto;

import lombok.Data;

/**
 * ip定位返回实体类
 * {
 * 	"status": "1",
 * 	"info": "OK",
 * 	"infocode": "10000",
 * 	"province": "山西省",
 * 	"city": "晋中市",
 * 	"adcode": "140700",
 * 	"rectangle": "112.6188648,37.59841362;112.8468096,37.81731902"
 * }
 */
@Data
public class IPDTO {

    //返回结果状态值  0表示失败；1表示成功
    private String status;
    //返回状态说明 status为0时，info返回错误原因，否则返回“OK”。
    private String info;
    //状态码 10000代表正确
    private String infocode;
    //省份名称
    private String province;
    //城市名称
    private String city;
    //城市的adcode编码
    private String adcode;
    //所在城市矩形区域范围
    private String rectangle;
}
