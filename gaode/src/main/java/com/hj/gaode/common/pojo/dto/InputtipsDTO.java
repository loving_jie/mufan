package com.hj.gaode.common.pojo.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 搜索提示返回实体类
 * {
 * 			"id": [],
 * 			"name": "太原市",
 * 			"district": "山西省太原市",
 * 			"adcode": "140107",
 * 			"location": [],
 * 			"address": "杏花岭区",
 * 			"typecode": "190104",
 * 			"city": []
 *                }
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class InputtipsDTO {
    //返回数据ID
    private String id;
    //tip名称
    private String name;
    //所属区域
    private String district;
    //区域编码
    private String adcode;
    //tip中心点坐标
    private String location;
    //详细地址
    private String address;
    private String typecode;
    private String city;

}
