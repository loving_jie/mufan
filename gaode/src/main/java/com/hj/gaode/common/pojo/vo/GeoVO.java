package com.hj.gaode.common.pojo.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 地理编码/逆地理编码入参
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class GeoVO {

    //结构化地址信息  必选
    private String address;

    //指定查询城市
    private String city;
}
