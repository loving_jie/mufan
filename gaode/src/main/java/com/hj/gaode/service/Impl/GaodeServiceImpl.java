package com.hj.gaode.service.Impl;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.hj.common.constant.AmapStatus;
import com.hj.common.exception.BusinessException;
import com.hj.common.pojo.model.AjaxResult;
import com.hj.common.util.HttpUtils;
import com.hj.common.util.StringUtils;
import com.hj.gaode.common.pojo.dto.GeoDTO;
import com.hj.gaode.common.pojo.dto.IPDTO;
import com.hj.gaode.common.pojo.dto.InputtipsDTO;
import com.hj.gaode.common.pojo.vo.GeoVO;
import com.hj.gaode.common.pojo.vo.InputtipsVO;
import com.hj.gaode.service.GaodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class GaodeServiceImpl implements GaodeService {

    @Autowired
    private HttpUtils http;


    @Override
    public List<GeoDTO> geo(GeoVO geoVO) {
        Map<String, Object> param = new HashMap<>();
        param.put("key", AmapStatus.appKey);
        param.put("address", geoVO.getAddress());
        //发送请求
        String result = http.doGet(AmapStatus.geoUrl, param);
        //处理请求
        JSONObject json = JSONObject.parseObject(result);
        if ("0".equals(json.getString("status"))) {//状态为 0 说明调用失败
            throw new BusinessException(result);//返回失败信息
        }
        List<GeoDTO> geocodeList = JSONObject.parseArray(json.getJSONArray("geocodes").toJSONString(), GeoDTO.class);
        return geocodeList;
    }

    @Override
    public String geo(String address) {
        GeoVO build = GeoVO.builder().address(address).build();
        List<GeoDTO> geo = geo(build);
        return geo.get(0).getLocation();
    }

    @Override
    public void walking(String begin, String end) {
        Map<String, Object> param = new HashMap<>();
        param.put("key", AmapStatus.appKey);
        param.put("origin", begin);
        param.put("destination", end);
        //发送请求
        String result = http.doGet(AmapStatus.geoUrl, param);
        //处理请求
        JSONObject json = JSONObject.parseObject(result);
        if ("0".equals(json.getString("status"))) {//状态为 0 说明调用失败
            throw new BusinessException(result);//返回失败信息
        }

    }


    @Override
    public IPDTO IPposition(String ip) {
        Map<String, Object> param = new HashMap<>();
        param.put("key", AmapStatus.appKey);
        param.put("ip", ip);
        //发送请求
        String result = http.doGet(AmapStatus.ipUrl, param);
        //处理返回值
        IPDTO ipdto = JSONObject.parseObject(result, IPDTO.class);
        if ("0".equals(ipdto.getStatus())) {
            throw new BusinessException(result);//返回失败信息
        }
        return ipdto;
    }

    @Override
    public List<InputtipsDTO> inputtips(InputtipsVO inputtipsVO) {
        Map<String, Object> param = new HashMap<>();
        param.put("key", AmapStatus.appKey);
        param.put("keywords", inputtipsVO.getKeywords());
        param.put("city", inputtipsVO.getCity());
        //发送请求
        String result = http.doGet(AmapStatus.inputtipsUrl, param);
        //处理返回值
        JSONObject json = JSONObject.parseObject(result);
        if ("0".equals(json.getString("status"))) {//状态为 0 说明调用失败
            throw new BusinessException(result);//返回失败信息
        }
        List<InputtipsDTO> inputtipsDTOList = JSONObject.parseArray(json.getJSONArray("tips").toJSONString(), InputtipsDTO.class);
        return inputtipsDTOList;
    }
}
