package com.hj.gaode.service;


import com.hj.gaode.common.pojo.dto.GeoDTO;
import com.hj.gaode.common.pojo.dto.IPDTO;
import com.hj.gaode.common.pojo.dto.InputtipsDTO;
import com.hj.gaode.common.pojo.vo.GeoVO;
import com.hj.gaode.common.pojo.vo.InputtipsVO;

import java.util.List;

public interface GaodeService {

    /**
     * 地理编码 :
     *      1.将详细的结构化地址转换为高德经纬度坐标。且支持对地标性名胜景区、建筑物名称解析为高德经纬度坐标。
     *          1.1 举例：北京市朝阳区阜通东大街6号转换后经纬度：116.480881,39.989410
     *      2.逆地理编码：将经纬度转换为详细结构化的地址，且返回附近周边的POI、AOI信息。
     *          2.1 例如：116.480881,39.989410 转换地址描述后：北京市朝阳区阜通东大街6号
     * @param geoVO
     */
    List<GeoDTO> geo(GeoVO geoVO);
    String geo(String address);
    //路径查询
    void walking(String begin, String end);
    //ip定位
    IPDTO IPposition(String ip);
    //输入提示
    List<InputtipsDTO> inputtips(InputtipsVO inputtipsVO);
}
