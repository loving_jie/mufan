package com.hj.common.pojo.dirctionary;

/**
 * 登陆状态
 */
public enum UserStatusEnum {
    OK("0", "正常"), DISABLE("1", "黑名单"), DELETED("2", "删除");


    private final String code;
    private final String info;

    UserStatusEnum(String code, String info) {
        this.code = code;
        this.info = info;
    }

    public String getCode() {
        return code;
    }

    public String getInfo() {
        return info;
    }
}
