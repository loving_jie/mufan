package com.hj.common.exception;

public class BusinessException extends RuntimeException {

	private static final long serialVersionUID = 5565760508056698922L;
	
	private Integer code;
	private String message;

	//TODO : 构造方法
	public BusinessException() {
	}

	public BusinessException(String message) {
		this.message = message;
	}

	public BusinessException(Integer code, String message) {
		this.code = code;
		this.message = message;
	}

	//TODO : get/set方法
	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	@Override
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
