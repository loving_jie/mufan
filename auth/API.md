## api : 
* 获取token : POST -->  127.0.0.1:9771/auth/user/login

    * body : 信息如下

```json
{
	"userName": "admin",
	"passWord": "123456"
}
```
* 获取用户信息 : GET -->  127.0.0.1:9771/auth/user/findByUser

* 退出登陆 : POST -->  127.0.0.1:9771/auth/logout

* 修改用户信息 : PUT  -->  127.0.0.1:9771/auth/user/updateUserById
    * body信息
```json
{
	"userId": "",
	"userName": "",
	"passWord": "",
	"userIco": "",
	"userSex": "",
	"userPhone": ""
}
```
* 如上接口需要在header里添加token信息 格式如下
```json
{
	"key": "Authorization",
	"value": "Bearer eyJhbGciOiJIUzUxMiJ9.eyJsb2dpbl91c2VyX2tleSI6ImRkMzVmMGMzODQzNTQzYmNiZDczZTY0MjhjMTc0ZTMxIn0.yLJktUpvH3iNxLXjJe16zSj-fLDHVdKq_mG1LDk7JqZ0NmT0Y9ohUei-tcHPiczWlmGOrXBMJTmCBDkYZlLOZg",
}
```