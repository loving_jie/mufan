package com.hj.auth.mapper;


import com.hj.auth.common.pojo.entity.User;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 用户表 数据层
 *
 * @author d
 * @date 2020-08-21 16:46:59
 */
@Repository
public interface UserMapper {

    //查询用户表信息
    public User selectUserById(Integer userId);

    // 查询用户表列表
    public List<User> selectUserList(User user);

    // 新增用户表
    public int insertUser(User user);

    //修改用户表
    public int updateUser(User user);

    //删除用户表
    public int deleteUserById(Integer userId);

    // 批量删除用户表
    public int deleteUserByIds(String[] userIds);

    User selectUserByUserName(String username);
}