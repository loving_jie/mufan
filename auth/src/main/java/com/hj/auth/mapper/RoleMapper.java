package com.hj.auth.mapper;

import com.hj.auth.common.pojo.entity.Role;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 *  数据层
 *
 * @author d
 * @date 2020-08-21 16:46:59
 */
@Repository
public interface RoleMapper {

    //查询信息
    public Role selectRoleById(Integer roleId);

    // 查询列表
    public List<Role> selectRoleList(Role role);

    // 新增
    public int insertRole(Role role);

    //修改
    public int updateRole(Role role);

    //删除
    public int deleteRoleById(Integer roleId);

    // 批量删除
    public int deleteRoleByIds(String[] roleIds);
}