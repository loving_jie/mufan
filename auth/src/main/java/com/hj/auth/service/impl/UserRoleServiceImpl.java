package com.hj.auth.service.impl;


import com.hj.auth.common.pojo.entity.UserRole;
import com.hj.auth.mapper.UserRoleMapper;
import com.hj.auth.service.UserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 *  服务层实现
 *
 * @author d
 * @date 2020-08-21 16:46:59
 */
@Service
public class UserRoleServiceImpl implements UserRoleService {

    @Autowired
    private UserRoleMapper userRoleMapper;

    //查询信息
    @Override
    public UserRole selectUserRoleById(Integer roleId) {
        return userRoleMapper.selectUserRoleById(roleId);
    }

    // 查询列表
    @Override
    public List<UserRole> selectUserRoleList(UserRole userRole) {
        return userRoleMapper.selectUserRoleList(userRole);
    }

    // 新增
    @Override
    public int insertUserRole(UserRole userRole) {
        return userRoleMapper.insertUserRole(userRole);
    }

    //修改
    @Override
    public int updateUserRole(UserRole userRole) {
        return userRoleMapper.updateUserRole(userRole);
    }

    //删除对象
    @Override
    public int deleteUserRoleByIds(String ids) {
        return userRoleMapper.deleteUserRoleByIds(ids.split(","));
    }
}