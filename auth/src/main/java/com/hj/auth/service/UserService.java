package com.hj.auth.service;


import com.hj.auth.common.pojo.entity.User;
import com.hj.auth.common.pojo.vo.UserVO;

import java.util.List;

/**
 * 用户表 服务层
 *
 * @author d
 * @date 2020-08-21 16:46:59
 */
public interface UserService {

    //校验用户信息 返回token
    String login(String userName, String passWord);

    //查询用户表信息
    public User selectUserById(Integer userId);

    //查询用户表列表
    public List<User> selectUserList(User user);

    //新增用户表
    public int insertUser(User user);

    //修改用户表
    public int updateUser(User user);

    //删除用户表信息
    public int deleteUserByIds(String ids);


}