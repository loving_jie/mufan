package com.hj.auth.service.impl;

import com.hj.auth.common.pojo.entity.Menu;
import com.hj.auth.mapper.MenuMapper;
import com.hj.auth.service.MenuService;
import com.hj.common.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 菜单权限表 服务层实现
 *
 * @author d
 * @date 2020-08-31 08:50:52
 */
@Service
public class MenuServiceImpl implements MenuService {

    @Autowired
    private MenuMapper menuMapper;


    //根据userid查询用户权限
    @Override
    public List<String> selectMenuByUserId(Integer userId) {
        List<String> list = menuMapper.selectMenuByUserId(userId);
        //存储非空权限
        List<String> permission = new ArrayList<>();
        for (String lis : list) {
            if (StringUtils.isNotEmpty(lis)){
                permission.add(lis);
            }
        }
        return permission;
    }

    //查询菜单权限表信息
    @Override
    public Menu selectMenuById(Integer menuId) {
        return menuMapper.selectMenuById(menuId);
    }

    // 查询菜单权限表列表
    @Override
    public List<Menu> selectMenuList(Menu menu) {
        return menuMapper.selectMenuList(menu);
    }

    // 新增菜单权限表
    @Override
    public int insertMenu(Menu menu) {
        return menuMapper.insertMenu(menu);
    }

    //修改菜单权限表
    @Override
    public int updateMenu(Menu menu) {
        return menuMapper.updateMenu(menu);
    }

    //删除菜单权限表对象
    @Override
    public int deleteMenuByIds(String ids) {
        return menuMapper.deleteMenuByIds(ids.split(","));
    }

}