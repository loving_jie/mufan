package com.hj.auth.service;


import com.hj.auth.common.pojo.entity.UserRole;

import java.util.List;

/**
 *  服务层
 *
 * @author d
 * @date 2020-08-21 16:46:59
 */
public interface UserRoleService {

    //查询信息
    public UserRole selectUserRoleById(Integer roleId);

    //查询列表
    public List<UserRole> selectUserRoleList(UserRole userRole);

    //新增
    public int insertUserRole(UserRole userRole);

    //修改
    public int updateUserRole(UserRole userRole);

    //删除信息
    public int deleteUserRoleByIds(String ids);
}