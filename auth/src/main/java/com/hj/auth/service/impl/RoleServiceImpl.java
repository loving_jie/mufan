package com.hj.auth.service.impl;


import com.hj.auth.common.pojo.entity.Role;
import com.hj.auth.mapper.RoleMapper;
import com.hj.auth.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 *  服务层实现
 *
 * @author d
 * @date 2020-08-21 16:46:59
 */
@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleMapper roleMapper;

    //查询信息
    @Override
    public Role selectRoleById(Integer roleId) {
        return roleMapper.selectRoleById(roleId);
    }

    // 查询列表
    @Override
    public List<Role> selectRoleList(Role role) {
        return roleMapper.selectRoleList(role);
    }

    // 新增
    @Override
    public int insertRole(Role role) {
        return roleMapper.insertRole(role);
    }

    //修改
    @Override
    public int updateRole(Role role) {
        return roleMapper.updateRole(role);
    }

    //删除对象
    @Override
    public int deleteRoleByIds(String ids) {
        return roleMapper.deleteRoleByIds(ids.split(","));
    }
}