package com.hj.auth.service;


import com.hj.auth.common.pojo.entity.Role;

import java.util.List;

/**
 *  服务层
 *
 * @author d
 * @date 2020-08-21 16:46:59
 */
public interface RoleService {

    //查询信息
    public Role selectRoleById(Integer roleId);

    //查询列表
    public List<Role> selectRoleList(Role role);

    //新增
    public int insertRole(Role role);

    //修改
    public int updateRole(Role role);

    //删除信息
    public int deleteRoleByIds(String ids);
}