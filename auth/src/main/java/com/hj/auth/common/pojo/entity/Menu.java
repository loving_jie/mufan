package com.hj.auth.common.pojo.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * 菜单权限表表 menu
 *
 * @author d
 * @date 2020-08-31 08:50:52
 */
@Data
public class Menu implements Serializable {
    private static final long serialVersionUID = 1L;

    //
    private Integer menuId;

    //目录名称 or 权限名称
    private String menuName;

    //父目录id
    private Integer menuParentId;

    //目录权限
    private String permission;

    //备注
    private String remark;

    //是否隐藏( 0 : 隐藏 1 : 显示)
    private String isRed;

}