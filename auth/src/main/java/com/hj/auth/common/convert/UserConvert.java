package com.hj.auth.common.convert;

import com.hj.auth.common.pojo.dto.UserDTO;
import com.hj.auth.common.pojo.entity.User;
import com.hj.auth.common.pojo.vo.UserVO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * mapstruct : user实体类转换
 */
@Mapper(componentModel = "spring")
public interface UserConvert {
    //转换类实例
    UserConvert INSTANCE = Mappers.getMapper(UserConvert.class);

    //entity转dto
    UserDTO entity2dot(User user);

    //vo转entity
    User vo2entity(UserVO userVO);
}
