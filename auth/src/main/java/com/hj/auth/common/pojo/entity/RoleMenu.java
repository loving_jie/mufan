package com.hj.auth.common.pojo.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * 角色 权限中间表表 role_menu
 *
 * @author d
 * @date 2020-08-31 08:50:52
 */
@Data
public class RoleMenu implements Serializable {
    private static final long serialVersionUID = 1L;

    //
    private Integer roleId;

    //
    private Integer menuId;

}