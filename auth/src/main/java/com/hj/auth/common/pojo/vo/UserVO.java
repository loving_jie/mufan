package com.hj.auth.common.pojo.vo;

import lombok.Data;

/**
 * 前端传参
 */
@Data
public class UserVO {

    private Integer userId;

    //用户名
    private String userName;

    //密码
    private String passWord;

    //用户头像
    private String userIco;

    //0 男 1 女 3 未知
    private String userSex;

    //手机号
    private String userPhone;
}
