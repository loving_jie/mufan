package com.hj.auth.common.pojo.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户表表 user  和数据库交互
 *
 * @author d
 * @date 2020-08-21 16:46:59
 */
@Data
public class User implements Serializable {
    private static final long serialVersionUID = 1L;

    //
    private Integer userId;

    //用户名
    private String userName;

    //密码
    private String passWord;

    //用户头像
    private String userIco;

    //0 男 1 女 3 未知
    private String userSex;

    //状态  0正常，1删除，2黑名单
    private String userStatus;

    //手机号
    private String userPhone;

    //创建时间
    private Date createTime;

    //类型
    private String userType;

}