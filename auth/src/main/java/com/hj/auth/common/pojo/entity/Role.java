package com.hj.auth.common.pojo.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 表 role
 *
 * @author d
 * @date 2020-08-21 16:46:59
 */
@Data
public class Role implements Serializable {
    private static final long serialVersionUID = 1L;

    //
    private Integer roleId;

    //角色名
    private String roleName;

    //状态 0 停用 1 正常
    private String roleStatus;

    //0 删除 1 正常
    private String delFlag;

    //创建时间
    private Date createTime;

    //备注
    private String remark;

}