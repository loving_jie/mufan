package com.hj.auth.common.config;

import com.hj.auth.common.filter.JwtAuthenticationTokenFilter;
import com.hj.auth.common.handler.MyAuthenticationEntryPoint;
import com.hj.auth.common.handler.MyLogoutSuccessHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

/**
 * security核心配置
 *
 * Spring Security默认是禁用注解的，要想开启注解，
 * 要在继承WebSecurityConfigurerAdapter的类加@EnableMethodSecurity注解，
 * 并在该类中将AuthenticationManager定义为Bean。
 */
@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true,securedEnabled=true,jsr250Enabled=true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private JwtAuthenticationTokenFilter jwtAuthenticationTokenFilter;//jwt校验过滤器

    @Autowired
    private MyLogoutSuccessHandler myLogoutSuccessHandler; //退出成功处理器

    @Autowired
    private MyAuthenticationEntryPoint myAuthenticationEntryPoint; //认证失败处理器

    @Override
    public void configure(WebSecurity web) throws Exception {
        //web.ignoring().antMatchers("/**");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http    //表单登陆
                .formLogin()
                // 由于使用的是JWT，我们这里不需要csrf
                .and().csrf().disable()
                // 认证失败处理类
                .exceptionHandling().authenticationEntryPoint(myAuthenticationEntryPoint).and()

                // 基于token，所以不需要session
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
                .authorizeRequests()

                //放过登陆接口(获取token)
                .antMatchers("/user/login").permitAll()//登陆接口
//                .antMatchers("/payment/callback/notyfyUrl").permitAll()//支付宝异步回调
//                .antMatchers("/payment/callback/returnUrll").permitAll()//支付宝同步回调
                .antMatchers("/**").permitAll()//所有请求  日常开发使用

                //所有请求全部需要认证；
                .anyRequest().authenticated()
        ;
        //退出登陆处理器
        http.logout().logoutUrl("/logout").logoutSuccessHandler(myLogoutSuccessHandler);

        // 禁用缓存
        http.headers().cacheControl();

        // 添加JWT filter  校验token信息
        http.addFilterBefore(jwtAuthenticationTokenFilter, UsernamePasswordAuthenticationFilter.class);
    }

    //加密处理bean
    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }

    //添加authenticationManagerBean对象
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }
}
