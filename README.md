
## 项目结构

* auth : 权限认证模块  security + jwt 

* common : 公共管理模块 

* eureka : 注册中心

* gatway : 网关

* quartz : 定时任务

* uoload : 上传下载

* sms : 短信发送 

* search : 整合elasticsearch-6.2.1

* payment : 支付宝支付接口

* ueditor : 百度富文本编辑器