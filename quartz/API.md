## 接口 : 
* 查询所有定时任务 : GET --> 127.0.0.1:9771/quartz/JobQuartz/selectList
* 添加定时任务 : POST --> 127.0.0.1:9771/quartz/JobQuartz/createJob
```json
{
	"jobName": "test",
	"description": "带参方法",
	"cronExpression": "0 */1 * * * ?",
	"expressionDesc": "每分钟执行一次",
	"beanClass": "jobQuartzController.test2(10086)",
	"jobGroup": "test",
	"concurrent": "0"
}
```
* 删除定时任务 : DELETE --> 127.0.0.1:9771/quartz/JobQuartz/delJob/{jobIds}
* 修改定时任务 : PUT  --> 127.0.0.1:9771/quartz/JobQuartz/updateJob
```json
{
    "id":"19",
	"jobName": "demo",
	"description": "带参方法",
	"cronExpression": "0 */1 * * * ?",
	"expressionDesc": "每分钟执行一次",
	"beanClass": "jobQuartzController.test2(10086)",
	"jobGroup": "test",
	"concurrent": "0"
}
```