package com.hj.quartz.common.pojo.vo;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.util.Date;

@Data
public class JobQuartzVO {


    private Integer id;
    /**
     *任务名
     */
    private String jobName;
    /**
     *任务描述
     */
    private String description;
    /**
     *cron表达式
     */
    private String cronExpression;
    /**
     *表达式描述
     */
    private String expressionDesc;
    /**
     *任务执行时调用哪个类的方法 类名 + 方法名
     */
    private String beanClass;
    /**
     *任务状态 0:停止 1:运行 2:暂停 -1:删除
     */
    private String jobStatus;
    /**
     *任务分组
     */
    private String jobGroup;
    /**
     * 是否并发执行 0 是  1 否
     */
    private String concurrent;
    /**
     *创建者
     */
    private String creator;
}
