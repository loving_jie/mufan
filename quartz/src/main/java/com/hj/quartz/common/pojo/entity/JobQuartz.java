package com.hj.quartz.common.pojo.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;


@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class JobQuartz {

    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;
    /**
     *任务名
     */
    private String jobName;
    /**
     *任务描述
     */
    private String description;
    /**
     *cron表达式
     */
    private String cronExpression;
    /**
     *表达式描述
     */
    private String expressionDesc;
    /**
     *任务执行时调用哪个类的方法 类名 + 方法名
     *  TODO : 注意 : 这里 类名首字母小写  在spring里注册的bean对象名称 首字母默认小写
     */
    private String beanClass;
    /**
     *任务状态 0:停止 1:运行 2:暂停 -1:删除
     */
    private String jobStatus;
    /**
     *任务分组
     */
    private String jobGroup;
    /**
     * 是否并发执行 0 是  1 否
     */
    private String concurrent;
    /**
     *创建者
     */
    private String creator;
    /**
     *创建时间
     */
    private Date createTime;

}
