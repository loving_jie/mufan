package com.hj.quartz.service;

import com.hj.quartz.common.pojo.entity.JobQuartz;

import java.util.List;

public interface JobQuartzService {

    List<JobQuartz> selectList();

    int createJob(JobQuartz jobQuartz);

    int deleteByJobIds(List<Integer> jobIds);

    int updateJob(JobQuartz jobQuartz);
}
