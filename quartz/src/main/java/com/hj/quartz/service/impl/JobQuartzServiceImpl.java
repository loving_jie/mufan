package com.hj.quartz.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.hj.common.pojo.dirctionary.QuartzStatusEnum;
import com.hj.quartz.common.manager.QuartzManager;
import com.hj.quartz.common.pojo.entity.JobQuartz;
import com.hj.quartz.mapper.JobQuartzMapper;
import com.hj.quartz.service.JobQuartzService;
import org.quartz.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Date;
import java.util.List;

@Service
public class JobQuartzServiceImpl implements JobQuartzService {

    @Autowired
    private JobQuartzMapper jobQuartzMapper;

    @Autowired
    private QuartzManager quartzManager;

    /**
     * 项目启动后 初始化定时任务
     */
    @PostConstruct//项目启动后 执行该方法
    public void init() throws SchedulerException {
        quartzManager.clear();
        QueryWrapper<JobQuartz> wrapper = new QueryWrapper<>();
        QueryWrapper<JobQuartz> ne = wrapper.eq("job_status", QuartzStatusEnum.RUN.getCode());
        List<JobQuartz> quartzList = jobQuartzMapper.selectList(ne);
        if (quartzList.size() != 0 && quartzList != null) {
            for (JobQuartz jobQuartz : quartzList) {
                quartzManager.addJob(jobQuartz);
            }
        }
    }

    //查询全部数据
    @Override
    public List<JobQuartz> selectList() {
        QueryWrapper<JobQuartz> wrapper = new QueryWrapper<>();
        QueryWrapper<JobQuartz> ne = wrapper.ne("job_status", QuartzStatusEnum.DELETE.getCode());
        return jobQuartzMapper.selectList(ne);
    }

    //创建任务
    @Override
    public int createJob(JobQuartz jobQuartz) {
        jobQuartz.setJobStatus(QuartzStatusEnum.RUN.getCode());//创建任务时为运行状态
        jobQuartz.setCreateTime(new Date());
        int insert = jobQuartzMapper.insert(jobQuartz);
        if (insert > 0){
            quartzManager.addJob(jobQuartz);
        }
        return insert;
    }

    @Override
    public int deleteByJobIds(List<Integer> jobIds) {
        //修改任务为状态为删除
        UpdateWrapper<JobQuartz> wrapper = new UpdateWrapper<>();
        wrapper.in("id",jobIds);//设置要修改的数据
        JobQuartz job = JobQuartz.builder().jobStatus(QuartzStatusEnum.DELETE.getCode()).build();
        int update = jobQuartzMapper.update(job, wrapper);

        //根据id批量查询任务信息
        List<JobQuartz> jobQuartzList = jobQuartzMapper.selectBatchIds(jobIds);
        for (JobQuartz jobQuartz : jobQuartzList) {
            quartzManager.deleteJob(jobQuartz.getId(), jobQuartz.getJobGroup());
        }
        return update;
    }

    @Override
    public int updateJob(JobQuartz jobQuartz) {
        jobQuartz.setJobStatus(QuartzStatusEnum.getCode(jobQuartz.getJobStatus()));
        int row = jobQuartzMapper.updateById(jobQuartz);
        if (row > 0){
            //如果修改状态为运行 则添加定时任务
            JobQuartz quartz = jobQuartzMapper.selectById(jobQuartz.getId());
            if (quartz.getJobStatus().equals(QuartzStatusEnum.RUN.getCode())) {
                quartzManager.addJob(quartz);
            }
        }
        return row;
    }


}
