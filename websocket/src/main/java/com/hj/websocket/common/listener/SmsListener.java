package com.hj.websocket.common.listener;

import com.aliyuncs.exceptions.ClientException;
import com.hj.common.constant.Constants;
import com.hj.common.constant.RabbitAttribute;
import com.hj.common.exception.BusinessException;
import com.hj.common.util.AliSmsUtils;
import com.hj.common.util.StringUtils;
import com.hj.common.util.UUIDUtils;
import com.hj.websocket.common.server.WebSocketServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * 监听队列发送短信
 */
@Component
public class SmsListener {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @RabbitHandler
    @RabbitListener(queues = RabbitAttribute.WEBSOCKET_QUEUE)
    public void smsListener(Map<String, Object> map) {
        if (StringUtils.isEmpty(map)){
            throw new BusinessException("websocket 消息为空");
        }
        String message = (String) map.get("message");
        String userId = (String)map.get("userId");
        //true : 指定用户发送  false : 群发
        boolean flag = (boolean)map.get("flag");
        try {
            if (flag) {
                WebSocketServer.sendInfo(message, userId);
            } else {
                WebSocketServer.sendInfoAll(message);
            }
        } catch (IOException e) {
            throw new BusinessException("websocket 消息发送失败");
        }
    }
}
