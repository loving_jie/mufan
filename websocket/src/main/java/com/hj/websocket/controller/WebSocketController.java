package com.hj.websocket.controller;

import com.hj.common.constant.RabbitAttribute;
import com.hj.common.pojo.model.AjaxResult;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class WebSocketController {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @GetMapping("/sendHtml")
    public AjaxResult sendHtml(String message, String userId) {
        Map<String, Object> map = new HashMap<>();
        map.put("message",message);
        map.put("userId",userId);
        map.put("flag",true);
        rabbitTemplate.convertAndSend(RabbitAttribute.DIRECT_EXCHANGE, RabbitAttribute.SEND_WEBSOCKET_ROUTINGKEY, map);
        return AjaxResult.success("发送成功");
    }
}
