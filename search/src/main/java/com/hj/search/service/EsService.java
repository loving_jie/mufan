package com.hj.search.service;


import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.search.SearchHits;

import java.util.Map;


public interface EsService {

    //创建索引
    boolean createIndex(String indexName, Integer shards, Integer replicas, Map<String, Map<String, String>> properties);

    //删除索引
    boolean deleteIndex(String indexName);

    //添加或全部更新文档
    Map<String, Object> insertDoc(String indexName, String type, String id, Object data);

    //删除文档
    String deleteDoc(String indexName, String type, String id);

    //修改文档
    Map<String, Object> localUpdateDoc(String indexName, String type, String id, Object data);

    //根据id搜索
    GetResponse findById(String indexName, String type, String id);

    //根据ids批量搜索
    SearchHits findByIds(String indexName, String type, String[] ids);
}
