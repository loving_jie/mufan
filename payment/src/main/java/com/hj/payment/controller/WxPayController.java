package com.hj.payment.controller;


import com.hj.common.exception.BusinessException;
import com.hj.common.pojo.model.AjaxResult;
import com.hj.common.util.QRCodeUtil;
import com.hj.payment.common.pojo.AliPayRefundVO;
import com.hj.payment.common.pojo.AliPayVO;
import com.hj.payment.common.pojo.AliTradePayVO;
import com.hj.payment.service.WxPayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;


/**
 * 微信支付
 */
@RestController
@RequestMapping("/wxpay")
public class WxPayController {
    
    @Autowired
    private WxPayService wxPayService;


    /**
     *  微信H5 支付
     * @param total_fee
     */
    @GetMapping("/WapPay")
    public void WapPay(String total_fee) {
        wxPayService.WapPay(total_fee);
    }

}
