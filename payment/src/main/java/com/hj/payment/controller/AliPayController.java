package com.hj.payment.controller;


import com.hj.common.exception.BusinessException;
import com.hj.common.pojo.model.AjaxResult;
import com.hj.common.util.QRCodeUtil;
import com.hj.payment.common.base.AliBaseParemIsNull;
import com.hj.payment.common.pojo.AliPayRefundVO;
import com.hj.payment.common.pojo.AliPayVO;
import com.hj.payment.common.pojo.AliTradePayVO;
import com.hj.payment.service.AliPayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

/**
 * 支付宝支付相关接口
 */
@RestController
@RequestMapping("/alipay")
public class AliPayController extends AliBaseParemIsNull {

    @Autowired
    private AliPayService aliPayService;

    @GetMapping("/aa")
    public String a(){
        return "111";
    }

    /**
     * 电脑网站支付
     *
     * @param aliPayVO
     * @return
     */
    @GetMapping("/PCPay")
    public void PCPay(AliPayVO aliPayVO) {
        AmountIsNull(aliPayVO.getAmount());
        aliPayService.pcPay(aliPayVO);
    }

    /**
     * 手机网站支付
     * @param aliPayVO
     */
    @GetMapping("/WapPay")
    public void WapPay(AliPayVO aliPayVO) {
        AmountIsNull(aliPayVO.getAmount());
        aliPayService.WapPay(aliPayVO);
    }

    /**
     * app支付
     * @param aliPayVO
     */
    @GetMapping("/AppPay")
    public AjaxResult AppPay(AliPayVO aliPayVO) {
        AmountIsNull(aliPayVO.getAmount());
        String str = aliPayService.appPay(aliPayVO);
        return AjaxResult.success("操作成功",str);
    }

    /**
     * 条码 声波支付  --> 商家扫码个人
     * @param aliTradePayVO
     * @return
     */
    @GetMapping("/tradePay")
    public AjaxResult tradePay(AliTradePayVO aliTradePayVO) {
        paramIsNull(aliTradePayVO);
        String response = aliPayService.tradePay(aliTradePayVO);
        return AjaxResult.success("操作成功",response);
    }

    /**
     * 扫码支付  -->  个人扫码商家
     * @param aliTradePayVO
     * @return
     */
    @GetMapping("/tradePrecreatePay")
    public AjaxResult tradePrecreatePay(AliTradePayVO aliTradePayVO) {
        storeIdIsNull(aliTradePayVO);
        String urlStr = aliPayService.tradePrecreatePay(aliTradePayVO);
        try {
            String qrCode = QRCodeUtil.createQRCode(urlStr, 500, 500);
            return AjaxResult.success("操作成功",qrCode);
        } catch (IOException e) {
            e.printStackTrace();
            throw new BusinessException("二维码生成失败");
        }
    }

    /**
     * 退款
     * @param aliPayRefundVO
     * @return
     */
    @GetMapping("/tradeRefund")
    public AjaxResult tradeRefund(AliPayRefundVO aliPayRefundVO){
        refundIsNull(aliPayRefundVO);
        String str = aliPayService.tradeRefund(aliPayRefundVO);
        return AjaxResult.success(str);
    }

    /**
     * 交易订单查询
     *
     * @param aliPayRefundVO
     * @return
     */
    @GetMapping("/tradeQuery")
    public AjaxResult tradeQuery(AliPayRefundVO aliPayRefundVO) {
        refundTwoIsNull(aliPayRefundVO);
        String str = aliPayService.tradeQuery(aliPayRefundVO);
        return AjaxResult.success("操作成功",str);
    }

}
