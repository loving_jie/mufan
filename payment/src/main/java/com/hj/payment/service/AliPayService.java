package com.hj.payment.service;

import com.hj.payment.common.pojo.AliPayRefundVO;
import com.hj.payment.common.pojo.AliPayVO;
import com.hj.payment.common.pojo.AliTradePayVO;

import javax.xml.ws.Response;

public interface AliPayService {

    //阿里电脑网站支付
    void pcPay(AliPayVO aliPayVO);

    //阿里app支付
    String appPay(AliPayVO aliPayVO);

    //手机网站支付
    void WapPay(AliPayVO aliPayVO);

    //条码 声波支付
    String tradePay(AliTradePayVO aliTradePayVO);

    //扫码支付
    String tradePrecreatePay(AliTradePayVO aliTradePayVO);

    //退款
    String tradeRefund(AliPayRefundVO aliPayRefundVO);

    //交易订单查询
    String tradeQuery(AliPayRefundVO aliPayRefundVO);
}
