package com.hj.payment.common.base;

import com.hj.common.exception.BusinessException;
import com.hj.common.util.StringUtils;
import com.hj.payment.common.pojo.AliPayRefundVO;
import com.hj.payment.common.pojo.AliPayVO;
import com.hj.payment.common.pojo.AliTradePayVO;

import java.math.BigDecimal;

public class AliBaseParemIsNull {

    //金额校验
    public void AmountIsNull(BigDecimal amount){
        if (StringUtils.isNull(amount)){
            throw new BusinessException("金额为空");
        }
    }


    //商户门店编号和金额校验
    public void storeIdIsNull(AliTradePayVO aliTradePayVO){
        if (StringUtils.isEmpty(aliTradePayVO.getStoreId())){
            throw new BusinessException("商户门店编号为空");
        }
        AmountIsNull(aliTradePayVO.getAmount());
    }

    //金额 授权码 商户编号 条码
    public void paramIsNull(AliTradePayVO aliTradePayVO){
        if (StringUtils.isEmpty(aliTradePayVO.getAuthCode())){
            throw new BusinessException("授权码为空");
        }
        if (StringUtils.isEmpty(aliTradePayVO.getScene())){
            throw new BusinessException("条码为空");
        }
        storeIdIsNull(aliTradePayVO);
    }

    public void refundIsNull(AliPayRefundVO aliPayRefundVO){
        if (StringUtils.isEmpty(aliPayRefundVO.getOutTradeNo()) && StringUtils.isEmpty(aliPayRefundVO.getTradeNo())){
            throw new BusinessException("订单号为空");
        }
        if (StringUtils.isEmpty(aliPayRefundVO.getFlag())){
            throw new BusinessException("是否全额退款标识为空");
        }
        AmountIsNull(aliPayRefundVO.getAmount());
    }

    public void refundTwoIsNull(AliPayRefundVO aliPayRefundVO){
        if (StringUtils.isEmpty(aliPayRefundVO.getOutTradeNo()) && StringUtils.isEmpty(aliPayRefundVO.getTradeNo())){
            throw new BusinessException("订单号为空");
        }
    }
}
