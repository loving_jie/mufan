package com.hj.uoload;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UoloadApplication {

    public static void main(String[] args) {
        SpringApplication.run(UoloadApplication.class, args);
        System.out.println("启动成功 !!!");
    }

}
