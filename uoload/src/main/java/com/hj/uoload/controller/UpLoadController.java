package com.hj.uoload.controller;


import com.google.gson.Gson;
import com.hj.common.designer.StrBuilder;
import com.hj.common.exception.BusinessException;
import com.hj.common.pojo.model.AjaxResult;
import com.hj.common.util.DateUtils;
import com.hj.common.util.QiNiuUtils;
import com.hj.common.util.StringUtils;
import com.hj.common.util.UUIDUtils;
import com.hj.uoload.common.util.VideoFrameUtils;
import com.qiniu.common.QiniuException;
import com.qiniu.http.Response;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.Region;
import com.qiniu.storage.UploadManager;
import com.qiniu.storage.model.DefaultPutRet;
import com.qiniu.util.Auth;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.*;

@RestController
@RequestMapping("/web")
public class UpLoadController {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Value("${upload.realpath}")
    private String realPath;

    /**
     * 七牛云上传文件
     * @param multipartFiles
     * @return
     */
    @PostMapping("/qiNiu")
    public AjaxResult qiNiu(@RequestParam("multipartFiles") MultipartFile[] multipartFiles) {
        //保存文件名称  key : 原文件名称  value : 生成文件名称
        Map<String, String> imageName = new HashMap<>();
        for (MultipartFile multipartFile : multipartFiles) {
            //获得上传文件的名称
            String name = multipartFile.getOriginalFilename();
            if (StringUtils.isEmpty(name)) {
                return AjaxResult.error("请选择文件");
            }
            logger.info("上传文件名称为 : {}", name);
            //获取文件后缀
            String suffix = StringUtils.substringAfterLast(name, ".");
            //生成唯一文件名称  并拼接上传文件的名称
            String fileName = getFileName(suffix);
            try {
                QiNiuUtils.upload2Qiniu(multipartFile.getBytes(),fileName);
            } catch (IOException e) {
                e.printStackTrace();
            }
            imageName.put(name, fileName);
        }
        return AjaxResult.success(imageName);
    }

    /**
     * 普通上传 : 上传到本地文件夹
     *
     * @param multipartFiles
     * @return
     */
    @PostMapping("/file")
    public AjaxResult file(@RequestParam("multipartFiles") MultipartFile[] multipartFiles) {
        try {
            //保存文件名称  key : 原文件名称  value : 生成文件名称
            Map<String, String> imageName = new HashMap<>();
            for (MultipartFile multipartFile : multipartFiles) {
                //获得上传文件的名称
                String name = multipartFile.getOriginalFilename();
                if (StringUtils.isEmpty(name)) {
                    return AjaxResult.error("请选择文件");
                }
                logger.info("上传文件名称为 : {}", name);
                //获取文件后缀
                String suffix = StringUtils.substringAfterLast(name, ".");
                //生成唯一文件名称  并拼接上传文件的名称
                String fileName = getFileName(suffix);
                //生成保存文件的路径
                File file = new File(realPath);
                //判断当前文件夹是否存在
                if (!file.exists()) {
                    //不存在就创建文件夹
                    file.mkdirs();
                }
                //上传文件
                multipartFile.transferTo(new File(file, fileName));
                imageName.put(name, fileName);
            }
            return AjaxResult.success("上传成功", imageName);
        } catch (IOException e) {
            throw new BusinessException("文件上传失败");
        }
    }


    /**
     * 生成文件名称
     *
     * @param suffix : 文件后缀名
     * @return
     */
    private String getFileName(String suffix) {
        String time = DateUtils.CurrentTimeFormat(new Date());//获取当前时间到毫秒值
        StringBuilder builder = StrBuilder.getStringBuilder();
        return builder.append(time).append(UUIDUtils.getRandomString(4)).append(UUIDUtils.getUuid()).append(".").append(suffix).toString();
    }

}
